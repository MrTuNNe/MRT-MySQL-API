package mrtunne.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MySQL
{
  private String Host;
  private String Database;
  private String Username;
  private String Password;
  private Connection connection;
  
  public MySQL(String host, String username, String password, String database)
  {
    this.Host = host;
    this.Database = database;
    this.Username = username;
    this.Password = password;
  }
  
  public void connect()
  {
    try
    {
      Class.forName("com.mysql.jdbc.Driver");
    }
    catch (ClassNotFoundException e1)
    {
      e1.printStackTrace();
    }
    String url = "jdbc:mysql://" + this.Host + ":3306/" + this.Database + "?autoReconnect=true";
    try
    {
      this.connection = DriverManager.getConnection(url, this.Username, this.Password);
    }
    catch (SQLException e2)
    {
      e2.printStackTrace();
    }
  }
  
  public void close()
  {
    try
    {
      if ((!this.connection.isClosed()) && (this.connection != null))
      {
        this.connection.close();
        System.out.println("[MySQL MRT] The connection to the MySQL server was successfully disconnected!");
      }
      else
      {
        System.out.println("[MySQL MRT] The connection is already disconnected!");
      }
    }
    catch (SQLException e3)
    {
      System.out.println("[MySQL MRT] There was an error while disconnecting!");
      e3.printStackTrace();
    }
  }
  
  public boolean isConnected()
  {
    try
    {
      if (this.connection.isClosed()) {
        return false;
      }
      return true;
    }
    catch (SQLException e2)
    {
      System.out.println("[MySQL MRT] An error occurred while connecting!");
      e2.printStackTrace();
    }
    return false;
  }
  
  public ResultSet fetch(String command)
  {
    try
    {
      if (this.connection.isClosed()) {
        connect();
      }
      Statement st = this.connection.createStatement();
      st.executeQuery(command);
      return st.getResultSet();
    }
    catch (SQLException e4)
    {
      System.out.println("[MySQL MRT] Error!");
      e4.printStackTrace();
    }
    return null;
  }

  public void query(String command)
  {
    try
    {
      if (this.connection.isClosed()) {
        connect();
      }
      Statement st = this.connection.createStatement();
      st.executeUpdate(command);
    }
    catch (SQLException e4)
    {
      e4.printStackTrace();
    }
  }
}
