# MRT-MySQL-API

This is an easy API for MySQL created by me from the #FreePortfolio Project.

This is a plugin API for Minecraft Server(Spigot Tested) & Simple Java

Can be used both without a problem :)

# REMEMBER

This isn't a Stable Version.

But you are free to use my API :) Is ok if you don't need something VERY big for moment. Have any suggestion? Great!

Go to "Issues" page and create a request :)

## MAYBE YOU NEED TO KNOW
- [Website](https://mrtunne.info/) - My website.


## HOW TO USE ? 

First, you have to import the package and also to add my .jar file to your external library.

After that, to connect your MySQL DB with Java use:

<b>MySQL mysql = new MySQL("hostname", "user", "password", "database");</b>

### But what about a query?

Well, if you are using my example, run into your Java code something like that:

<b>mysql.query("YOUR SQL CODE");</b>

#### How can I get a result?

You can use something like that:

<b>mysql.fetch("YOUR SQL CODE");</b>

More updates will coming soon! Just stay up to date with my API :)
